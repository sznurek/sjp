module Examples where

import Syntax

-- Examples.
fac :: Integer -> Exp
fac n = LetRec "fac" "n"
         (If (Lt (Var "n") (NatConst 2))
             (NatConst 1)
             (Mult (Var "n")
                   (App (Var "fac")
                        (Minus (Var "n") (NatConst 1)))))
         (App (Var "fac") (NatConst n))

escape = CallCC
           (Abs "k" (Plus (NatConst 2)
                          (Throw (Var "k") (Mult (NatConst 3) (NatConst 4)))))

divZero = Div (NatConst 42) (NatConst 0)

backtrack = Reset (Plus (NatConst 1)
                        (Shift (Abs "c"
                               (Mult (Throw (Var "c") (NatConst 3))
                                     (Throw (Var "c") (NatConst 5))))))

abort = Reset (Plus (NatConst 1)
                    (Shift (Abs "c" (NatConst 10))))
