\documentclass[10pt,leqno,fleqn]{article}

\usepackage[utf8]{inputenc}
\usepackage[OT4]{fontenc}
\usepackage{amssymb}
\usepackage[polish]{babel}

\usepackage{a4wide}

\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{bm}
\usepackage{syntax}
 
\newtheorem{thm}{Twierdzenie}

% Kropka po numerze paragrafu, podparagrafu itp. 
\makeatletter
 \renewcommand\@seccntformat[1]{\csname the#1\endcsname.\quad}
 \renewcommand\numberline[1]{#1.\hskip0.7em}
\makeatother

% Inna numeracja wzorów.
\renewcommand{\theequation}{\arabic{section}.\arabic{equation}}

% Semantics brackets.
\newsavebox{\sembox}
\newlength{\semwidth}
\newlength{\boxwidth}

\newcommand{\Sem}[1]{%
\sbox{\sembox}{\ensuremath{#1}}%
\settowidth{\semwidth}{\usebox{\sembox}}%
\sbox{\sembox}{\ensuremath{\left[\usebox{\sembox}\right]}}%
\settowidth{\boxwidth}{\usebox{\sembox}}%
\addtolength{\boxwidth}{-\semwidth}%
\left[\hspace{-0.3\boxwidth}%
\usebox{\sembox}%
\hspace{-0.3\boxwidth}\right]%
}

\DeclareMathOperator{\get}{get}
\DeclareMathOperator{\apply}{apply}
\DeclareMathOperator{\applyCont}{applyCont}
\DeclareMathOperator{\applyMCont}{applyMCont}

\newcommand{\lname}[1]{\mathop{\kern0pt\mathtt{#1}}}

\title{Defunkcjonalizacja semantyki denotacyjnej języka z operatorami \texttt{shift} i \texttt{reset}
}
\author{Łukasz Dąbek}
\date{Polkowice, dnia \today\ r.}

\begin{document}
\thispagestyle{empty}
\maketitle
\newpage

\section{Wstęp}
Przedstawiony gorliwy język funkcyjny jest rozszerzeniem języka przedstawionego w
jedenastym rozdziale książki Reynoldsa ,,Theories of programming languages'' o operatory \texttt{shift}
i \texttt{reset}.

\section{Składnia języka}
\begin{grammar}
<natconst> ::= 0 | 1 | 2 | $\cdots$

<boolconst> ::= true | false

<exp> ::= <natconst>
    \alt <boolconst>
    \alt - <exp>
    \alt $\neg$ <exp>
    \alt <exp> + <exp>
    \alt <exp> - <exp>
    \alt <exp> * <exp>
    \alt <exp> / <exp>
    \alt <exp> `or' <exp>
    \alt <exp> `and' <exp>
    \alt <exp> `eq' <exp>
    \alt <exp> `lt' <exp>
    \alt `if' <exp> `then' <exp> `else' <exp>
    \alt <var>
    \alt $\lambda$<var>.<exp>
    \alt <exp> <exp>
    \alt `letrec' <var> = $\lambda$<var>.<exp> in <exp>
    \alt `shift' <exp>
    \alt `reset' <exp>
    \alt `throw' <exp> <exp>
    \alt `error'
    \alt `typeerror'
\end{grammar}

\section{Nieformalny opis semantyki}

Semantyka większości konstrukcji językowych jest taka sama jak w książce Reynoldsa, także
kilku słów opisu wymagają jedynie konstrukcje \texttt{shift} i \texttt{reset}.

Wyrażenie $\lname{reset} e$ pozwala na pozbycie się bieżącego kontekstu obliczeń z kontynuacji
i przełożenie go do meta kontynuacji. Instrukcja \texttt{reset} ogranicza kontynuacje pobrane
przez \texttt{shift} w wyrażeniu $e$.

Wyrażenie $\lname{shift} e$ ma sens wtedy, gdy $e$ jest funkcją. Będzie ona zaaplikowana do
bieżącej kontynuacji, którą można następnie ''rzucić'' przy użyciu wyrażenia \texttt{throw}.

\section{Konwencje}
Stosowane są następujące meta zmienne:
\begin{itemize}
    \item $i$ oznacza liczby całkowite,
    \item $n$ oznacza liczby naturalne,
    \item $b$ oznacza stałe logiczne,
    \item $e$ oznacza wyrażenie,
    \item $\eta$ oznacza środowisko,
    \item $\kappa$ oznacza kontynuację,
    \item $\gamma$ oznacza meta kontynuację,
    \item $x, f, y$ oznacza zmienną,
\end{itemize}

Każda z meta zmiennych może występować z ozdobnikami (np. subskrypt). Zmienna z daszkiem
(np. $\hat{x}$) oznacza obiekt ze świata semantyki pierwszorzędowej.

Dodatkowo, $Exp$ oznacza zbiór wszystkich wyrażeń rozważanego języka, $Var$ oznacza zbiór
zmiennych.

\section{Semantyka wyższorzędowa}
\subsection{Dziedziny i pradziedziny}
\begin{align*}
    V &\xleftrightarrow[\phi]{\psi} V_{int} + V_{bool} + V_{fun} + V_{cont} + V_{mcont} \\
    V_{int}   &= \mathbb{Z} \\
    V_{bool}  &= \{ \text{true}, \text{false} \} \\
    V_{fun}   &= V \rightarrow V_{cont} \rightarrow V_{mcont} \rightarrow V_* \\
    V_{cont}  &= V_{mcont} \rightarrow V_{mcont} \\
    V_{mcont} &= V \rightarrow V_* \\
    V_*       &= (V + \{ \text{error}, \text{typeerror} \})_{\bot} \\
    E         &= Var \rightarrow V
\end{align*}

\subsection{Włożenia i inne funkcje pomocnicze}
Włożenia $\iota_{-}$ są zdefiniowane dla każdej z powyższych pradziedzin. Dodatkowo
używamy włożenia $\iota_{norm} : V \rightarrow V_*$ oraz stałych $\lname{err}$ i $\lname{tyerr}$
analogicznie jak w książce Reynoldsa.

\subsection{Semantyka wyższorzędowa}
\begin{align*}
    \Sem{-} : Exp &\rightarrow E \rightarrow V_{cont} \rightarrow V_{mcont} \rightarrow V_* \\
    \Sem{\lname{error}} \eta \kappa \gamma     &=
        ~\lname{err} \\
    \Sem{\lname{typeerror}} \eta \kappa \gamma &=
        ~\lname{tyerr} \\
    \Sem{\lfloor n \rfloor} \eta \kappa \gamma &=
        \iota_{int}~n \\
    \Sem{\lfloor b \rfloor} \eta \kappa \gamma &=
        \iota_{bool}~b \\
    \Sem{-e} \eta \kappa \gamma               &=
        ~\Sem{e} \eta~(\lambda i. \kappa~\gamma~(-i))_{int}~\gamma \\
    \Sem{\neg e} \eta \kappa \gamma           &=
        ~\Sem{e} \eta~(\lambda b. \kappa~\gamma~(\neg b))_{bool}~\gamma \\
    \Sem{e_0 + e_1} \eta \kappa \gamma        &=
        ~\Sem{e_0} \eta~(\lambda \gamma. (\lambda i_0. \Sem{e_1} \eta~(\lambda \gamma. (\lambda i_1. \kappa~\gamma~(i_0 + i_1))_{int})~\gamma)_{int})~\gamma \\
    \Sem{e_0 - e_1} \eta \kappa \gamma        &=
        ~\Sem{e_0} \eta~(\lambda \gamma. (\lambda i_0. \Sem{e_1} \eta~(\lambda \gamma. (\lambda i_1. \kappa~\gamma~(i_0 - i_1))_{int})~\gamma)_{int})~\gamma \\
    \Sem{e_0 * e_1} \eta \kappa \gamma        &=
        ~\Sem{e_0} \eta~(\lambda \gamma. (\lambda i_0. \Sem{e_1} \eta~(\lambda \gamma. (\lambda i_1. \kappa~\gamma~(i_0 \cdot i_1))_{int})~\gamma)_{int})~\gamma \\
    \Sem{e_0 / e_1} \eta \kappa \gamma        &=
        ~\Sem{e_0} \eta~(\lambda \gamma. (\lambda i_0. \Sem{e_1} \eta~(\lambda \gamma. (\lambda i_1. \lname{cond}(i_1 == 0, \lname{err}, \kappa~\gamma~(i_0 / i_1)))_{int})~\gamma)_{int})~\gamma \\
    \Sem{e_0~\lname{or}~e_1} \eta \kappa \gamma        &=
        ~\Sem{e_0} \eta~(\lambda \gamma. (\lambda b_0. \Sem{e_1} \eta~(\lambda \gamma. (\lambda b_1. \kappa~\gamma~(b_0 \lor b_1))_{bool})~\gamma)_{bool})~\gamma \\
    \Sem{e_0~\lname{and}~e_1} \eta \kappa \gamma        &=
        ~\Sem{e_0} \eta~(\lambda \gamma. (\lambda b_0. \Sem{e_1} \eta~(\lambda \gamma. (\lambda b_1. \kappa~\gamma~(b_0 \land b_1))_{bool})~\gamma)_{bool})~\gamma \\
    \Sem{e_0~\lname{eq}~e_1} \eta \kappa \gamma        &=
        ~\Sem{e_0} \eta~(\lambda \gamma. (\lambda i_0. \Sem{e_1} \eta~(\lambda \gamma. (\lambda i_1. \kappa~\gamma~(i_0 == i_1))_{int})~\gamma)_{int})~\gamma \\
    \Sem{e_0~\lname{lt}~e_1} \eta \kappa \gamma        &=
        ~\Sem{e_0} \eta~(\lambda \gamma. (\lambda i_0. \Sem{e_1} \eta~(\lambda \gamma. (\lambda i_1. \kappa~\gamma~(i_0 < i_1))_{int})~\gamma)_{int})~\gamma \\
    \Sem{\lname{if}~e~\lname{then}~e_0~\lname{else}~e_1} \eta \kappa \gamma &=
        ~\Sem{e} \eta~(\lambda \gamma. (\lambda b. \lname{cond}(b, \Sem{e_0} \eta~\kappa~\gamma, \Sem{e_1} \eta~\kappa~\gamma) )_{bool})~\gamma \\
    \Sem{\lname{letrec}~f = \lambda x.e'~\lname{in}~e} \eta \kappa \gamma &=
        ~\Sem{e} (\eta[f := \iota_{fun}(\lname{fix}~(\lambda g~z~\kappa~\gamma. \Sem{e'} (\eta[x := z][f := \iota_{fun}~g])~\kappa~\gamma)]))~\kappa~\gamma \\
    \Sem{x} \eta \kappa \gamma                &=
        ~\kappa~\gamma~(\eta~x) \\
    \Sem{\lambda x.e} \eta \kappa \gamma      &=
        ~\kappa~\gamma~(\iota_{fun}~(\lambda v~\kappa~\gamma. \Sem{e} \eta[x:=v]~\kappa~\gamma)) \\
    \Sem{e_0~e_1} \eta \kappa \gamma          &=
        ~\Sem{e_0} \eta~(\lambda \gamma. (\lambda f. \Sem{e_1} \eta~(\lambda \gamma~v. f~v~\kappa~\gamma)~\gamma)_{fun})~\gamma \\
    \Sem{\lname{throw}~e_0~e_1} \eta \kappa \gamma    &=
        ~\Sem{e_0} \eta~(\lambda \gamma. (\lambda \kappa'. \Sem{e_1} \eta~\kappa'~(\kappa \gamma))_{cont})~\gamma \\
    \Sem{\lname{shift}~e} \eta \kappa \gamma          &=
        ~\Sem{e} \eta~(\lambda \gamma. (\lambda f. f~(\iota_{cont}~k)~\lname{id}~\gamma)_{fun})~\gamma \\
    \Sem{\lname{reset}~e} \eta \kappa \gamma          &=
        ~\Sem{e} \eta~\lname{id}~(\kappa~\gamma)
\end{align*}

\section{Semantyka pierwszorzędowa}
\subsection{Dziedziny i pradziedziny}
\begin{align*}
    \hat{V} &\xleftrightarrow[\phi]{\psi} \hat{V}_{int} + \hat{V}_{bool} + \hat{V}_{fun} + \hat{V}_{cont} + \hat{V}_{mcont} \\
    \hat{V}_{int}   &= \mathbb{Z} \\
    \hat{V}_{bool}  &= \{ \lname{true}, \lname{false} \} \\
    \hat{V}_{fun}   &= \{\lname{abstract}\} \times Var \times Exp \times \hat{E}\\
    \hat{V}_{mcont} &= \{\lname{mempty}\} + \{\lname{mpush}\} \times \hat{V}_{cont} \times \hat{V}_{mcont} \\
    \hat{V}_*       &= (V + \{ \lname{error}, \lname{typeerror} \})_{\bot} \\
    \hat{E}         &= \{\lname{emptyenv}\} + \{\lname{extend}\} \times Var \times \hat{V} \times \hat{E} +
        \{\lname{recenv}\} \times \hat{E} \times Var \times Var \times Exp \\
\end{align*}

\begin{align*}
    \hat{V}_{cont} &= \{\lname{cid}\}\\
                   &+ \{\lname{cnegate}\} \times \hat{V}_{cont} \\
                   &+ \{\lname{cnot}\} \times \hat{V}_{cont} \\
                   &+ \{\lname{cand1}\} \times Exp \times \hat{E} \times \hat{V}_{cont} \\
                   &+ \{\lname{cand2}\} \times \hat{V}_{bool} \times \hat{V}_{cont} \\
                   &+ \{\lname{cor1}\} \times Exp \times \hat{E} \times \hat{V}_{cont} \\
                   &+ \{\lname{cor2}\} \times \hat{V}_{bool} \times \hat{V}_{cont} \\
                   &+ \{\lname{cplus1}\} \times Exp \times \hat{E} \times \hat{V}_{cont} \\
                   &+ \{\lname{cplus2}\} \times \hat{V}_{int} \times \hat{V}_{cont} \\
                   &+ \{\lname{cminus1}\} \times Exp \times \hat{E} \times \hat{V}_{cont} \\
                   &+ \{\lname{cminus2}\} \times \hat{V}_{int} \times \hat{V}_{cont} \\
                   &+ \{\lname{cmult1}\} \times Exp \times \hat{E} \times \hat{V}_{cont} \\
                   &+ \{\lname{cmult2}\} \times \hat{V}_{int} \times \hat{V}_{cont} \\
                   &+ \{\lname{cdiv1}\} \times Exp \times \hat{E} \times \hat{V}_{cont} \\
                   &+ \{\lname{cdiv2}\} \times \hat{V}_{int} \times \hat{V}_{cont} \\
                   &+ \{\lname{ceq1}\} \times Exp \times \hat{E} \times \hat{V}_{cont} \\
                   &+ \{\lname{ceq2}\} \times \hat{V}_{int} \times \hat{V}_{cont} \\
                   &+ \{\lname{clt1}\} \times Exp \times \hat{E} \times \hat{V}_{cont} \\
                   &+ \{\lname{clt2}\} \times \hat{V}_{int} \times \hat{V}_{cont} \\
                   &+ \{\lname{cif}\} \times Exp \times Exp \times \hat{E} \times \hat{V}_{cont} \\
                   &+ \{\lname{capp1}\} \times Exp \times \hat{E} \times \hat{V}_{cont} \\
                   &+ \{\lname{capp2}\} \times \hat{V}_{fun} \times \hat{V}_{cont} \\
                   &+ \{\lname{cshift}\} \times \hat{V}_{cont} \\
                   &+ \{\lname{cthrow}\} \times Exp \times \hat{E} \times \hat{V}_{cont} \\
\end{align*}

\subsection{Włożenia i inne funkcje pomocnicze}
Sprawdzanie typów jest rozwiązane tak jak w przypadku semantyki wyższorzędowej. Wszystkie włożenia
do odpowiednich dziedzin i pradziedzin zdefiniowane są analogicznie jak poprzednio.

\subsection{Semantyka}

\begin{align*}
    &\get : \hat{E} \rightarrow Var \rightarrow \hat{V} \\
    &\get~\lname{emptyenv}~x     = \iota_{int} 0 \\
    &\get~\langle \lname{extend}~y~v~e \rangle~x = \lname{cond}(x == y, v, \get e~x) \\
    &\get~\langle \lname{recenv}~e~f~x~b \rangle ~x = \lname{cond}(x == y, \iota_{fun}~\langle \lname{abstract}, x, b, \langle \lname{recenv}, e, f, x, b\rangle \rangle, \get e~x) \\
\end{align*}

\begin{align*}
    &\applyMCont : \hat{V}_{mcont} \rightarrow \hat{V} \rightarrow \hat{V}_* \\
    &\applyMCont~\lname{mid}~v = \iota_{norm} v \\
    &\applyMCont~\langle \lname{mpush}~\kappa~\gamma \rangle~v = \applyCont \kappa~\gamma~v \\
\end{align*}

\begin{align*}
    &\applyCont : \hat{V}_{cont} \rightarrow \hat{V}_{mcont} \rightarrow \hat{V} \rightarrow \hat{V}_* \\
    &\applyCont~\lname{cid}~\gamma~v = \applyMCont \gamma~v \\
    &\applyCont~\langle \lname{cnegate}, \kappa \rangle~\gamma~v = (\lambda i. \applyCont~\kappa~\gamma~(-i))_{int}~v \\
    &\applyCont~\langle \lname{cnot}, \kappa \rangle~\gamma~v = (\lambda b. \applyCont~\kappa~\gamma~(\neg b))_{bool}~v \\
    &\applyCont~\langle \lname{cand1}, e, \eta, \kappa \rangle~\gamma~v = (\lambda b. \Sem{e}\eta~\langle \lname{cand2}, b, \kappa \rangle~\gamma)_{bool}~v \\
    &\applyCont~\langle \lname{cand2}, b, \kappa \rangle~\gamma~v = (\lambda b'. \applyCont \kappa~\gamma~(b \land b'))_{bool}~v \\
    &\applyCont~\langle \lname{cor1}, e, \eta, \kappa \rangle~\gamma~v = (\lambda b. \Sem{e}\eta~\langle \lname{cor2}, b, \kappa \rangle~\gamma)_{bool}~v \\
    &\applyCont~\langle \lname{cor2}, b, \kappa \rangle~\gamma~v = (\lambda b'. \applyCont \kappa~\gamma~(b \lor b'))_{bool}~v \\
    &\applyCont~\langle \lname{cplus1}, e, \eta, \kappa \rangle~\gamma~v = (\lambda i. \Sem{e}\eta~\langle \lname{cplus2}, i, \kappa \rangle~\gamma)_{int}~v \\
    &\applyCont~\langle \lname{cplus2}, i, \kappa \rangle~\gamma~v = (\lambda i'. \applyCont \kappa~\gamma~(i + i'))_{int}~v \\
    &\applyCont~\langle \lname{cminus1}, e, \eta, \kappa \rangle~\gamma~v = (\lambda i. \Sem{e}\eta~\langle \lname{cminus2}, i, \kappa \rangle~\gamma)_{int}~v \\
    &\applyCont~\langle \lname{cminus2}, i, \kappa \rangle~\gamma~v = (\lambda i'. \applyCont \kappa~\gamma~(i - i'))_{int}~v \\
    &\applyCont~\langle \lname{cmult1}, e, \eta, \kappa \rangle~\gamma~v = (\lambda i. \Sem{e}\eta~\langle \lname{cmult2}, i, \kappa \rangle~\gamma)_{int}~v \\
    &\applyCont~\langle \lname{cmult2}, i, \kappa \rangle~\gamma~v = (\lambda i'. \applyCont \kappa~\gamma~(i \cdot i'))_{int}~v \\
    &\applyCont~\langle \lname{cdiv1}, e, \eta, \kappa \rangle~\gamma~v = (\lambda i. \Sem{e}\eta~\langle \lname{cdiv2}, i, \kappa \rangle~\gamma)_{int}~v \\
    &\applyCont~\langle \lname{cdiv2}, i, \kappa \rangle~\gamma~v = (\lambda i'. \lname{cond}(i' == 0, \lname{err}, \applyCont \kappa~\gamma~(i / i')))_{int}~v \\
    &\applyCont~\langle \lname{ceq1}, e, \eta, \kappa \rangle~\gamma~v = (\lambda i. \Sem{e}\eta~\langle \lname{ceq2}, i, \kappa \rangle~\gamma)_{int}~v \\
    &\applyCont~\langle \lname{ceq2}, i, \kappa \rangle~\gamma~v = (\lambda i'. \applyCont \kappa~\gamma~(i == i'))_{int}~v \\
    &\applyCont~\langle \lname{clt1}, e, \eta, \kappa \rangle~\gamma~v = (\lambda i. \Sem{e}\eta~\langle \lname{clt2}, i, \kappa \rangle~\gamma)_{int}~v \\
    &\applyCont~\langle \lname{clt2}, i, \kappa \rangle~\gamma~v = (\lambda i'. \applyCont \kappa~\gamma~(i < i'))_{int}~v \\
    &\applyCont~\langle \lname{capp1}, e, \eta, \kappa \rangle~\gamma~v = (\lambda f. \Sem{e} \eta~\langle \lname{capp2}, f, \kappa \rangle~\gamma)_{fun}~v \\
    &\applyCont~\langle \lname{capp2}, f, \kappa \rangle~\gamma~v = \lname{apply} f~v~\kappa~\gamma \\
    &\applyCont~\langle \lname{cthrow}, e, \eta, \kappa \rangle~\gamma~v = (\lambda \kappa'. \Sem{e} \eta~\kappa'~\langle \lname{mpush}, \kappa, \gamma \rangle)_{cont}~v \\
    &\applyCont~\langle \lname{cshift}, \kappa \rangle~\gamma~v = (\lambda f. \lname{apply} f~(\iota_{cont}~\kappa)~\lname{cid}~\gamma)_{fun}~v \\
\end{align*}

\begin{align*}
    &\lname{apply} : \hat{V}_{fun} \rightarrow \hat{V} \rightarrow \hat{V}_{cont} \rightarrow \hat{V}_{mcont} \rightarrow \hat{V}_* \\
    &\lname{apply} \langle \lname{abstract}, x, e, \eta \rangle~v~\kappa~\gamma = \Sem{e} \eta~\langle \lname{extend}, \eta, x, v \rangle~\kappa~\gamma
\end{align*}

\begin{align*}
    \Sem{-} : Exp &\rightarrow \hat{E} \rightarrow \hat{V}_{cont} \rightarrow \hat{V}_{mcont} \rightarrow \hat{V}_* \\
    \Sem{\lname{error}} \eta \kappa \gamma     &=
        ~\lname{err} \\
    \Sem{\lname{typeerror}} \eta \kappa \gamma &=
        ~\lname{tyerr} \\
    \Sem{\lfloor n \rfloor} \eta \kappa \gamma &=
        ~\applyCont \kappa~\gamma~(\iota_{int}~n) \\
    \Sem{\lfloor b \rfloor} \eta \kappa \gamma &=
        ~\applyCont \kappa~\gamma~(\iota_{bool}~b) \\
    \Sem{- e} \eta \kappa \gamma              &=
        ~\Sem{e} \eta~\langle \lname{cnegate}, \kappa \rangle~ \gamma \\
    \Sem{\neg e} \eta \kappa \gamma           &=
        ~\Sem{e} \eta~\langle \lname{cnot}, \kappa \rangle~ \gamma \\
    \Sem{e_0 + e_1} \eta \kappa \gamma        &=
        ~\Sem{e_0} \eta~\langle \lname{cadd1}, e_1, \eta, \kappa \rangle~\gamma \\ 
    \Sem{e_0 - e_1} \eta \kappa \gamma        &=
        ~\Sem{e_0} \eta~\langle \lname{cminus1}, e_1, \eta, \kappa \rangle~\gamma \\ 
    \Sem{e_0 * e_1} \eta \kappa \gamma        &=
        ~\Sem{e_0} \eta~\langle \lname{cmult1}, e_1, \eta, \kappa \rangle~\gamma \\ 
    \Sem{e_0 / e_1} \eta \kappa \gamma        &=
        ~\Sem{e_0} \eta~\langle \lname{cdiv1}, e_1, \eta, \kappa \rangle~\gamma \\ 
    \Sem{e_0~\lname{or}~e_1} \eta \kappa \gamma        &=
        ~\Sem{e_0} \eta~\langle \lname{cor1}, e_1, \eta, \kappa \rangle~\gamma \\ 
    \Sem{e_0~\lname{and}~e_1} \eta \kappa \gamma        &=
        ~\Sem{e_0} \eta~\langle \lname{cand1}, e_1, \eta, \kappa \rangle~\gamma \\ 
    \Sem{e_0~\lname{eq}~e_1} \eta \kappa \gamma        &=
        ~\Sem{e_0} \eta~\langle \lname{ceq1}, e_1, \eta, \kappa \rangle~\gamma \\ 
    \Sem{e_0~\lname{lt}~e_1} \eta \kappa \gamma        &=
        ~\Sem{e_0} \eta~\langle \lname{clt1}, e_1, \eta, \kappa \rangle~\gamma \\ 
    \Sem{\lname{if}~e~\lname{then}~e_0~\lname{else}~e_1} \eta \kappa \gamma &=
        ~\Sem{e} \eta~\langle \lname{cif}, e_0, e_1, \eta, \kappa \rangle~\gamma \\
    \Sem{\lname{letrec}~f = \lambda x.e'~\lname{in}~e} \eta \kappa \gamma &=
        ~\Sem{e} \langle \lname{recenv}, \eta, f, x, e' \rangle~\kappa~\gamma \\
    \Sem{x} \eta \kappa \gamma                &=
        ~\applyCont \kappa~\gamma~(\get \eta~x) \\
    \Sem{\lambda x.e} \eta \kappa \gamma      &=
        ~\applyCont \kappa~\gamma~(\iota_{fun}~\langle \lname{abstract}, x, e, \eta \rangle) \\
    \Sem{e_0~e_1} \eta \kappa \gamma          &=
        ~\Sem{e_0} \eta~\langle \lname{capp1}, e_1, \eta, \kappa \rangle~\gamma \\ 
    \Sem{\lname{throw}~e_0~e_1} \eta \kappa \gamma    &=
        ~\Sem{e_0} \eta~\langle \lname{cthrow}, e_1, \eta, \kappa \rangle~\gamma \\ 
    \Sem{\lname{shift}~e} \eta \kappa \gamma          &=
        ~\Sem{e} \eta~\langle \lname{cshift}, \kappa \rangle~\gamma \\ 
    \Sem{\lname{reset}~e} \eta \kappa \gamma          &=
        ~\Sem{e} \eta~\lname{cid}~\langle \lname{mpush}, \kappa, \gamma \rangle \\ 
\end{align*}

\section{Relacja pomiędzy semantykami}
\begin{enumerate}
    \item $\langle \hat{i}, i \rangle \in R_{int} \subseteq \hat{V}_{int} \times V_{int}$ wtedy i tylko wtedy, gdy
        $\hat{i} = i$.
    \item $\langle \hat{b}, b \rangle \in R_{bool} \subseteq \hat{V}_{bool} \times V_{bool}$ wtedy i tylko wtedy, gdy
        $\hat{b} = b$.
    \item $\langle \hat{f}, f \rangle \in R_{fun} \subseteq \hat{V}_{fun} \times V_{fun}$ wtedy i tylko wtedy, gdy
        $\langle \hat{z}, z \rangle \in R$, $\langle \hat{\kappa}, \kappa \rangle \in R_{cont}$, $\langle \hat{\gamma}, \gamma \rangle \in R_{mcont}$ i
        $\langle \apply \hat{f}~\hat{z}~\hat{\kappa}~\hat{\gamma}, f~z~\kappa~\gamma \rangle \in R_*$.
    \item $\langle \hat{\kappa}, \kappa \rangle \in R_{cont} \subseteq \hat{V}_{cont} \times V_{cont}$ wtedy i tylko wtedy, gdy
        $\langle \hat{z}, z \rangle \in R$, $\langle \hat{\gamma}, \gamma \rangle \in R_{mcont}$ i $\langle \applyCont \hat{\kappa}~\hat{\gamma}~\hat{z},
        \kappa ~ \gamma ~ z \rangle \in R_*$.
    \item $\langle \hat{\gamma}, \gamma \rangle \in R_{mcont} \subseteq \hat{V}_{mcont} \times V_{mcont}$ wtedy i tylko wtedy, gdy
        $\langle \hat{z}, z \rangle \in R$ i $\langle \applyMCont \hat{\gamma}~\hat{z}, \gamma~z \rangle \in R_{mcont}$.
    \item $\langle \hat{z}, z \rangle \in R \subseteq \hat{V} \times V$ wtedy i tylko wtedy, gdy
        dla pewnego $\theta \in \{int,bool,fun,cont,mcont\}$ i $\langle \hat{x}, x \rangle \in R_\theta$ jest
        $\hat{z} = \iota_{\theta}~\hat{x}$ oraz $z = \iota_{\theta}~x$.
    \item $\langle \hat{x}, x \rangle \in R_* \subseteq \hat{V}_* \times V_*$ wtedy i tylko wtedy, gdy
        $\langle \hat{z}, z \rangle \in R$ i $\hat{x} = \iota_{norm}~\hat{z}$ $x = \iota_{norm} z$ lub
        $\hat{x} = x = tyerr$ lub $\hat{x} = x = err$ lub $\hat{x} = x = \bot$.
    \item $\langle \hat{\eta}, \eta \rangle \in R_E \subseteq \hat{E} \times E$ wtedy i tylko wtedy, gdy
        dla każdej zmiennej $v$, $\langle \lname{get} \hat{\eta}~v, \eta~v \rangle \in R$.
\end{enumerate}

\noindent
Dla tak zdefiniowanej relacji możemy sformułować twierdzenie o równoważności przedstawionych semantyk:

\begin{thm}
    Niech $\langle \hat{\kappa}, \kappa \rangle \in R_{cont}$, 
    $\langle \hat{\gamma}, \gamma \rangle \in R_{mcont}$ oraz $\langle \hat{\eta}, \eta \rangle \in R_E$.
    Wtedy dla każdego $e \in Exp$ mamy $\langle \Sem{e}_{fo} \hat{\eta}~\hat{\kappa}~\hat{\gamma}, \Sem{e} \eta~\kappa~\gamma \rangle \in R_*$,
    gdzie $\Sem{-}_{fo}$ oznacza pierwszorzędową funkcję semantyczną.
\end{thm}

\end{document}

