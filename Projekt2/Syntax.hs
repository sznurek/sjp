module Syntax where

type Var = String

data Exp = NatConst Integer
         | BoolConst Bool
         | Negate Exp
         | Not Exp
         | And Exp Exp
         | Or Exp Exp
         | Eq Exp Exp
         | Lt Exp Exp
         | Plus Exp Exp
         | Minus Exp Exp
         | Mult Exp Exp
         | Div Exp Exp
         | If Exp Exp Exp
         | Var Var
         | Abs Var Exp
         | App Exp Exp
         | LetRec Var Var Exp Exp -- name, abstraction var, abstraction body, rest of expression
         | CallCC Exp
         | Throw Exp Exp
         | Reset Exp
         | Shift Exp
         | ErrorConst
         | TypeErrorConst deriving (Show)

