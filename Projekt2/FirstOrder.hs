module FirstOrder where

import Syntax

-- Domains and predomains definitions.
type VInt   = Integer
type VBool  = Bool
type VFun   = (Var, Exp, Env)

data VCont  = CId
            | CNegate VCont
            | CNot VCont
            | CAnd1 Exp Env VCont
            | CAnd2 VBool VCont
            | COr1 Exp Env VCont
            | COr2 VBool VCont
            | CEq1 Exp Env VCont
            | CEq2 VInt VCont
            | CLt1 Exp Env VCont
            | CLt2 VInt VCont
            | CPlus1 Exp Env VCont
            | CPlus2 VInt VCont
            | CMinus1 Exp Env VCont
            | CMinus2 VInt VCont
            | CMult1 Exp Env VCont
            | CMult2 VInt VCont
            | CDiv1 Exp Env VCont
            | CDiv2 VInt VCont
            | CIf Exp Exp Env VCont
            | CApp1 Exp Env VCont
            | CApp2 VFun VCont
            | CThrow Exp Env VCont
            | CShift VCont
            | CCallCC VCont

data VMCont = MId
            | MPush VCont VMCont

data V = VBool VBool
       | VInt VInt
       | VFun VFun
       | VCont VCont

instance Show V where
    show (VBool b) = "VBool " ++ show b
    show (VInt i)  = "VInt " ++ show i
    show (VFun _)  = "VFun"
    show (VCont _) = "VCont"

data VStar = Norm V
           | Error
           | TypeError deriving (Show)

data Env = InitEnv
         | Extend Env Var V
         | RecEnv Env Var Var Exp

get :: Env -> Var -> V
get InitEnv _          = VInt 0
get (Extend e x v) y   = if x == y then v else get e y
get (RecEnv e f x b) y = if y == f then VFun (x, b, RecEnv e f x b) else get e y

-- Dynamic typechecking.
fInt :: (VInt -> VStar) -> V -> VStar
fInt f (VInt i) = f i
fInt _ _        = TypeError

fBool :: (VBool -> VStar) -> V -> VStar
fBool f (VBool i) = f i
fBool _ _         = TypeError

fFun :: (VFun -> VStar) -> V -> VStar
fFun f (VFun i) = f i
fFun _ _        = TypeError

fCont :: (VCont -> VStar) -> V -> VStar
fCont f (VCont i) = f i
fCont _ _         = TypeError

-- Semantics.

applyMCont :: VMCont -> V -> VStar
applyMCont MId v         = Norm v
applyMCont (MPush k m) v = applyCont k m v

applyCont CId m v               = applyMCont m v
applyCont (CNegate k) m v       = fInt  (\i -> applyCont k m (VInt (-i))) v
applyCont (CNot k) m v          = fBool (\b -> applyCont k m (VBool (not b))) v
applyCont (CAnd1 exp e k) m v   = fBool (\b -> eval exp e (CAnd2 b k) m) v
applyCont (CAnd2 b k) m v       = fBool (\b' -> applyCont k m (VBool (b && b'))) v
applyCont (COr1 exp e k) m v    = fBool (\b -> eval exp e (COr2 b k) m) v
applyCont (COr2 b k) m v        = fBool (\b' -> applyCont k m (VBool (b || b'))) v
applyCont (CEq1 exp e k) m v    = fInt  (\i -> eval exp e (CEq2 i k) m) v
applyCont (CEq2 i k) m v        = fInt  (\i' -> applyCont k m (VBool (i == i'))) v
applyCont (CLt1 exp e k) m v    = fInt  (\i -> eval exp e (CLt2 i k) m) v
applyCont (CLt2 i k) m v        = fInt  (\i' -> applyCont k m (VBool (i < i'))) v
applyCont (CPlus1 exp e k) m v  = fInt  (\i -> eval exp e (CPlus2 i k) m) v
applyCont (CPlus2 i k) m v      = fInt  (\i' -> applyCont k m (VInt (i + i'))) v
applyCont (CMinus1 exp e k) m v = fInt  (\i -> eval exp e (CMinus2 i k) m) v
applyCont (CMinus2 i k) m v     = fInt  (\i' -> applyCont k m (VInt (i - i'))) v
applyCont (CMult1 exp e k) m v  = fInt  (\i -> eval exp e (CMult2 i k) m) v
applyCont (CMult2 i k) m v      = fInt  (\i' -> applyCont k m (VInt (i * i'))) v
applyCont (CDiv1 exp e k) m v   = fInt  (\i -> eval exp e (CDiv2 i k) m) v
applyCont (CDiv2 i k) m v       = fInt  (\i' -> if i' == 0 then Error else applyCont k m (VInt (i `div` i'))) v
applyCont (CIf e1 e2 e k) m v   = fBool (\b -> if b then eval e1 e k m else eval e2 e k m) v
applyCont (CApp1 arg e k) m v   = fFun  (\f -> eval arg e (CApp2 f k) m) v
applyCont (CApp2 f k) m v       = apply f v k m
applyCont (CThrow exp e k) m v  = fCont (\k' -> eval exp e k' (MPush k m)) v
applyCont (CShift k) m v        = fFun  (\f -> apply f (VCont k) CId m) v
applyCont (CCallCC k) m v       = fFun  (\f -> apply f (VCont k) k m) v

apply :: VFun -> V -> VCont -> VMCont -> VStar
apply (x, exp, e) v k m = eval exp (Extend e x v) k m

eval :: Exp -> Env -> VCont -> VMCont -> VStar
eval ErrorConst e k m     = Error
eval TypeErrorConst e k m = TypeError

eval (NatConst n)  e k m = applyCont k m (VInt n)
eval (BoolConst b) e k m = applyCont k m (VBool b)

eval (Negate exp) e k m = eval exp e (CNegate k) m
eval (Not exp) e k m    = eval exp e (CNot k) m

eval (And exp1 exp2) e k m = eval exp1 e (CAnd1 exp2 e k) m
eval (Or  exp1 exp2) e k m = eval exp1 e (COr1  exp2 e k) m
eval (Eq  exp1 exp2) e k m = eval exp1 e (CEq1  exp2 e k) m
eval (Lt  exp1 exp2) e k m = eval exp1 e (CLt1  exp2 e k) m

eval (Plus  exp1 exp2) e k m = eval exp1 e (CPlus1  exp2 e k) m
eval (Minus exp1 exp2) e k m = eval exp1 e (CMinus1 exp2 e k) m
eval (Mult  exp1 exp2) e k m = eval exp1 e (CMult1  exp2 e k) m
eval (Div   exp1 exp2) e k m = eval exp1 e (CDiv1   exp2 e k) m

eval (If cond exp1 exp2) e k m = eval cond e (CIf exp1 exp2 e k) m

eval (Var x) e k m         = applyCont k m (get e x)
eval (Abs x exp) e k m     = applyCont k m (VFun (x, exp, e))
eval (App exp1 exp2) e k m = eval exp1 e (CApp1 exp2 e k) m

eval (LetRec f x body exp) e k m = eval exp (RecEnv e f x body) k m

eval (Throw exp1 exp2) e k m = eval exp1 e (CThrow exp2 e k) m

eval (CallCC exp) e k m = eval exp e (CCallCC k) m
eval (Reset exp) e k m  = eval exp e CId (MPush k m)
eval (Shift exp) e k m  = eval exp e (CShift k) m

