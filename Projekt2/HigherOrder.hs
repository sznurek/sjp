module HigherOrder where

import Syntax

-- (Really) small arsenal for domain theory
fix :: (a -> a) -> a
fix f = f (fix f)

-- Domains and predomains definitions.
type VInt   = Integer
type VBool  = Bool
type VFun   = V -> VCont -> VMCont -> VStar
type VCont  = VMCont -> VMCont
type VMCont = V -> VStar

data V = VBool VBool
       | VInt VInt
       | VFun VFun
       | VCont VCont

instance Show V where
    show (VBool b) = "VBool " ++ show b
    show (VInt i)  = "VInt " ++ show i
    show (VFun _)  = "VFun"
    show (VCont _) = "VCont"

data VStar = Norm V
           | Error
           | TypeError deriving (Show)

type Env = Var -> V


-- Environment handling.
emptyEnv :: Env
emptyEnv _ = VInt 0

updateEnv :: Env -> Var -> V -> Env
updateEnv e x v = \y -> if x == y then v else e y

-- Dynamic typechecking.
fInt :: (VInt -> VStar) -> V -> VStar
fInt f (VInt i) = f i
fInt _ _        = TypeError

fBool :: (VBool -> VStar) -> V -> VStar
fBool f (VBool i) = f i
fBool _ _         = TypeError

fFun :: (VFun -> VStar) -> V -> VStar
fFun f (VFun i) = f i
fFun _ _        = TypeError

fCont :: (VCont -> VStar) -> V -> VStar
fCont f (VCont i) = f i
fCont _ _         = TypeError

-- Semantics.
eval :: Exp -> Env -> VCont -> VMCont -> VStar
eval ErrorConst e k m     = Error
eval TypeErrorConst e k m = TypeError

eval (NatConst n)  e k m = k m (VInt n)
eval (BoolConst b) e k m = k m (VBool b)

eval (Negate exp) e k m = eval exp e (\m -> fInt  (\i -> k m (VInt (-i)))) m
eval (Not exp) e k m    = eval exp e (\m -> fBool (\b -> k m (VBool (not b)))) m

eval (And exp1 exp2) e k m = eval exp1 e (\m -> fBool (\b1 -> eval exp2 e (\m -> fBool (\b2 -> k m (VBool (b1 && b2)))) m)) m
eval (Or  exp1 exp2) e k m = eval exp1 e (\m -> fBool (\b1 -> eval exp2 e (\m -> fBool (\b2 -> k m (VBool (b1 || b2)))) m)) m
eval (Eq  exp1 exp2) e k m = eval exp1 e (\m -> fInt  (\i1 -> eval exp2 e (\m -> fInt  (\i2 -> k m (VBool (i1 == i2)))) m)) m
eval (Lt  exp1 exp2) e k m = eval exp1 e (\m -> fInt  (\i1 -> eval exp2 e (\m -> fInt  (\i2 -> k m (VBool (i1 <  i2)))) m)) m

eval (Plus  exp1 exp2) e k m = eval exp1 e (\m -> fInt (\i1 -> eval exp2 e (\m -> fInt (\i2 -> k m (VInt (i1 + i2)))) m)) m
eval (Minus exp1 exp2) e k m = eval exp1 e (\m -> fInt (\i1 -> eval exp2 e (\m -> fInt (\i2 -> k m (VInt (i1 - i2)))) m)) m
eval (Mult  exp1 exp2) e k m = eval exp1 e (\m -> fInt (\i1 -> eval exp2 e (\m -> fInt (\i2 -> k m (VInt (i1 * i2)))) m)) m
eval (Div   exp1 exp2) e k m = eval exp1 e (\m -> fInt (\i1 -> eval exp2 e (\m -> fInt (\i2 -> if i2 == 0 then Error else k m (VInt (i1 `div` i2)))) m)) m

eval (If cond exp1 exp2) e k m = eval cond e (\m -> fBool (\b -> if b then eval exp1 e k m else eval exp2 e k m)) m

eval (Var x) e k m         = k m (e x)
eval (Abs x exp) e k m     = k m (VFun (\v k' m' -> eval exp (updateEnv e x v) k' m'))
eval (App exp1 exp2) e k m = eval exp1 e (\m -> fFun (\f -> eval exp2 e (\m v -> f v k m) m)) m

eval (LetRec f x body exp) e k m = eval exp e' k m where
    e' = updateEnv e f (VFun fixed)
    fixed = fix (\g z k m -> eval body (updateEnv (updateEnv e x z) f (VFun g)) k m)

eval (Throw exp1 exp2) e k m = eval exp1 e (\m -> fCont (\k' -> eval exp2 e k' (k m))) m

eval (CallCC exp) e k m = eval exp e (\m -> fFun (\f -> f (VCont k) k m)) m
eval (Reset exp) e k m  = eval exp e id (k m)
eval (Shift exp) e k m  = eval exp e (\m -> fFun (\f -> f (VCont k) id m)) m

