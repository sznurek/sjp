module Syntax where

data Arith = AConst Integer
           | Var String
           | Add Arith Arith
           | Sub Arith Arith
           | Mul Arith Arith
           deriving (Eq, Show, Read)

data Boolean = BConst Bool
             | And Boolean Boolean
             | Or Boolean Boolean
             | Not Boolean
             | Eq Arith Arith
             | Lt Arith Arith
             deriving (Eq, Show, Read)

data Command = Skip
             | Assign String Arith
             | Seq Command Command
             | If Boolean Command Command
             | While Boolean Command
             | Fail
             | LFail String
             | Catch String Command Command
             | Output Arith
             | Read String
             | Goto String
             | Block [(String, Command)]
             deriving (Eq, Show, Read)

