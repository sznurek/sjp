module Environment where

type Env = String -> Integer

emptyEnv :: Env
emptyEnv = const 0

updateEnv :: Env -> String -> Integer -> Env
updateEnv e v n = \v' -> if v == v' then n else e v'
