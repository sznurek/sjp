module ExprSemantics where

import Syntax
import Environment

aeval :: Env -> Arith -> Integer
beval :: Env -> Boolean -> Bool

aeval _ (AConst n) = n
aeval e (Var s) = e s
aeval e (Add a1 a2) = aeval e a1 + aeval e a2
aeval e (Sub a1 a2) = aeval e a1 - aeval e a2
aeval e (Mul a1 a2) = aeval e a1 * aeval e a2

beval _ (BConst b) = b
beval e (And b1 b2) = beval e b1 && beval e b2
beval e (Or b1 b2)  = beval e b1 || beval e b2
beval e (Not b) = not (beval e b)
beval e (Eq a1 a2) = aeval e a1 == aeval e a2
beval e (Lt a1 a2) = aeval e a1 < aeval e a2

