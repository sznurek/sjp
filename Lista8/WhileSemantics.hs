module WhileSemantics where

import Syntax
import ExprSemantics
import Environment

data D = State (Maybe Env)
       | DOutput Integer D
       | DInput (Integer -> D)

star :: (Env -> D) -> (D -> D)
star f (State Nothing) = State Nothing
star f (State (Just e)) = f e
star f (DOutput n d) = DOutput n (star f d)
star f (DInput g) = DInput (\n -> star f (g n))

fixpoint :: (a -> a) -> a
fixpoint f = f (fixpoint f)

iState :: Env -> D
iState = State . Just

eval :: Command -> Env -> D
eval Skip e         = iState e
eval (Assign v a) e = iState (updateEnv e v (aeval e a))
eval (Seq c0 c1) e  = (star (eval c1)) (eval c0 e)
eval (If b c0 c1) e = if beval e b then eval c0 e else eval c1 e
eval (While b c) e  = fixpoint f e where
    f g e = if beval e b
              then (star g) (eval c e)
              else iState e
eval (Fail) _       = State Nothing
eval (Output a) e   = DOutput (aeval e a) (iState e))
eval (Read x) e     = DInput (\n -> iState (updateEnv e x n)))

evalc :: Command -> (Env -> D) -> (Env -> D)
evalc Skip k e = k e
evalc (Assign v a) k e = k (updateEnv e v (aeval e a))
evalc (Seq c0 c1) k e = evalc c0 (evalc c1 k) e
evalc (If b c0 c1) k e = if beval e b then evalc c0 k e else evalc c1 k e
evalc (While b c) k e  = fixpoint f e where
    f g e = if beval e b
              then evalc c g e
              else k e
evalc (Fail) _ _ = State Nothing
evalc (Output a) k e = DOutput (aeval e a) (k e)
evalc (Read v) k e = DInput (\n -> k (updateEnv e v n))

runcont c e = evalc c iState e

fac = Seq (Seq
            (Assign "x" (AConst 1))
            (While (Lt (AConst 0) (Var "n")) (Seq
                                                (Assign "x" (Mul (Var "x") (Var "n")))
                                                (Assign "n" (Sub (Var "n") (AConst 1))))))
          (Output (Var "x"))

cat = While (BConst True) (Seq (Read "x") (Output (Var "x")))

interpret :: D -> IO ()
interpret (State Nothing) = putStrLn "FAIL" >> return ()
interpret (State (Just e)) = putStrLn "State" >> return ()
interpret (DOutput n d) = putStrLn (show n) >> interpret d
interpret (DInput f) = fmap read getLine >>= interpret . f
