module WhileSemantics3 where

import Syntax
import ExprSemantics
import Environment

import Debug.Trace

type D = Env

fixpoint :: (a -> a) -> a
fixpoint f = f (fixpoint f)

updateLabel :: (String -> Env -> D) -> String -> (Env -> D) -> String -> Env -> D
updateLabel j l e = \l' -> if l == l' then e else j l'

evalc :: Command -> (String -> Env -> D) -> (Env -> D) -> Env -> D
evalc Skip _ k e = k e
evalc (Assign v a) kp ks e = ks (updateEnv e v (aeval e a))
evalc (Seq c0 c1) kp ks e  = evalc c0 kp (evalc c1 kp ks) e
evalc (If b c0 c1) kp ks e = if beval e b
                                then evalc c0 kp ks e
                                else evalc c1 kp ks e
evalc (While b c) kp ks e  = fixpoint f e where
    f g e = if beval e b
              then (evalc c kp g e)
              else e
evalc (Goto l) kp ks e   = kp l e
evalc (Block cs) kp ks e = snd (head envs) e where
    lastI = length cs - 1
    envs = fixpoint f
    f bs = map (\(i,(l,c)) -> (l, \e -> evalc c (updateCont kp bs) (getNext bs i) e)) $ zip [0..] cs
    updateCont k [] = k
    updateCont k ((l,c):bs) = updateLabel (updateCont k bs) l c
    getNext bs k = if k == lastI then ks else snd (bs !! (k+1))

fac = Seq (Assign "x" (AConst 1)) $
          Block [
            ("start", If (Lt (Var "n") (AConst 1)) (Goto "end") (Skip)),
            ("step", Seq (Assign "x" (Mul (Var "x") (Var "n"))) (Assign "n" (Sub (Var "n") (AConst 1)))),
            ("jump", Goto "start"),
            ("end", Skip) ]

noopBlock = Block [
              ("start", Skip),
              ("stop", Skip) ]

runcont c e = evalc c (\l e -> e) (\e -> e) e
