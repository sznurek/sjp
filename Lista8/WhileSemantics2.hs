module WhileSemantics2 where

import Syntax
import ExprSemantics
import Environment

data D = Pure Env
       | Failed String Env

instance Show D where
    show (Pure _) = "Pure _"
    show (Failed l _) = "Failed " ++ l ++ " _"

star :: (Env -> D) -> (D -> D)
star f (Pure e) = f e
star f (Failed l e) = Failed l e

fixpoint :: (a -> a) -> a
fixpoint f = f (fixpoint f)

eval :: Command -> Env -> D
eval Skip e            = Pure e
eval (Assign v a) e    = Pure (updateEnv e v (aeval e a))
eval (Seq c0 c1) e     = (star (eval c1)) (eval c0 e)
eval (If b c0 c1) e    = if beval e b then eval c0 e else eval c1 e
eval (While b c) e     = fixpoint f e where
    f g e = if beval e b
              then (star g) (eval c e)
              else Pure e
eval (LFail l) e       = Failed l e
eval (Catch l c0 c1) e = case eval c0 e of
                            Pure e -> Pure e
                            Failed l' e -> if l == l' then eval c1 e else Failed l' e

evalc :: Command -> (String -> Env -> D) -> (Env -> D) -> Env -> D
evalc Skip _ k e = k e
evalc (Assign v a) kp ks e = ks (updateEnv e v (aeval e a))
evalc (Seq c0 c1) kp ks e  = evalc c0 kp (evalc c1 kp ks) e
evalc (If b c0 c1) kp ks e = if beval e b
                                then evalc c0 kp ks e
                                else evalc c1 kp ks e
evalc (While b c) kp ks e  = fixpoint f e where
    f g e = if beval e b
              then (evalc c kp g e)
              else Pure e
evalc (LFail l) kp ks e = kp l e
evalc (Catch l c0 c1) kp ks e = evalc c0 (\l' -> if l == l' then evalc c1 kp ks else kp l') ks e

runcont c e = evalc c Failed Pure e

catchme = Catch "onie" (LFail "onie") Skip
dontcatchme = Catch "onie2" (LFail "onie") Skip

