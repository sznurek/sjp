module Common where

type Ide = String
type Loc = Int
type Env = Ide -> Loc
type Mem a = (Loc, Loc -> a)

emptyMem :: Mem a
emptyMem = (0, \_ -> error "Location not in memory")

emptyEnv :: Env
emptyEnv _ = error "Variable not in environment"

updateEnv :: Env -> Ide -> Loc -> Env
updateEnv e x l = \x' -> if x == x' then l else e x'

firstFree :: Mem a -> Loc
firstFree = fst

dealloc :: Mem a -> Loc -> Mem a
dealloc (_, m) l = (l, m)

alloc :: Mem a -> a -> (Loc, Mem a)
alloc (n, m) a = (n, (n+1, \l -> if l == n then a else m l))

updateMem :: Mem a -> Loc -> a -> Mem a
updateMem (n, m) l a = (n, \l' -> if l == l' then a else m l')

memGet :: Mem a -> Loc -> a
memGet (_, m) l = m l

data Aexp = Literal Integer
          | Var Ide
          | Add Aexp Aexp
          | Sub Aexp Aexp
          | Mul Aexp Aexp

data Bexp = Not Bexp
          | And Bexp Bexp
          | Lt Aexp Aexp
          | Eq Aexp Aexp

data Decl = VarDecl Ide Aexp
          | ProcDecl Ide Ide Command

data Command = Skip
             | Assign Ide Aexp
             | Seq Command Command
             | If Bexp Command Command
             | While Bexp Command
             | Block [Decl] Command
             | Call Ide Aexp

