import Common
import Debug.Trace

data Storable = Number Integer | Procedure (Integer -> Mem Storable -> D)
type D = Maybe (Env, Mem Storable)

fromNumber :: Storable -> Integer
fromNumber (Number n) = n
fromNumber _ = error "fromNumber failed"

fromProcedure :: Storable -> (Integer -> Mem Storable -> D)
fromProcedure (Procedure p) = p
fromProcedure _ = error "fromProcedure failed"

aeval :: Aexp -> Env -> Mem Storable -> Integer
aeval (Literal n) e m = n
aeval (Var x) e m = fromNumber (memGet m (e x))
aeval (Add a1 a2) e m = aeval a1 e m + aeval a2 e m
aeval (Sub a1 a2) e m = aeval a1 e m - aeval a2 e m
aeval (Mul a1 a2) e m = aeval a1 e m * aeval a2 e m

beval :: Bexp -> Env -> Mem Storable -> Bool
beval (Not b) e m = not (beval b e m)
beval (And b1 b2) e m = beval b1 e m && beval b2 e m
beval (Lt a1 a2) e m = aeval a1 e m < aeval a2 e m
beval (Eq a1 a2) e m = aeval a1 e m == aeval a2 e m

evalDecl :: [Decl] -> Env -> Mem Storable -> (Env, Mem Storable)
evalDecl [] e m = (e, m)
evalDecl (VarDecl x a:ds) e m = evalDecl ds (updateEnv e x p) m' where
    (p, m') = alloc m (Number n)
    n = aeval a e m
evalDecl (ProcDecl p n c:ds) e m = evalDecl ds (updateEnv e p s) m' where
    (s, m') = alloc m (Procedure (fix f))

    f :: (Integer -> Mem Storable -> D) -> (Integer -> Mem Storable -> D)
    f g i m = eval c (updateEnv (updateEnv e n argp) p s) (updateMem m'' s (Procedure g)) where
        (argp, m'') = alloc m (Number i)

star :: (Mem Storable -> D) -> (D -> D)
star f (Just (_, m)) = f m
star _ Nothing = Nothing

fix :: (a -> a) -> a
fix f = f (fix f)

eval :: Command -> Env -> Mem Storable -> D
eval Skip e m = Just (e, m)
eval (Assign x a) e m = Just (e, updateMem m (e x) (Number $ aeval a e m))
eval (Seq c1 c2) e m = star (eval c2 e) (eval c1 e m)
eval (If b c1 c2) e m = if cond then eval c1 e m else eval c2 e m where
    cond = beval b e m
eval (While b c) e m = fix f m where
    f :: (Mem Storable -> D) -> (Mem Storable -> D)
    f g m = if beval b e m
              then star g (eval c e m)
              else Just (e, m)
eval (Call p a) e m = fromProcedure (memGet m (e p)) (aeval a e m) m
eval (Block ds c) e m = deallocation (eval c e' m') where
    p = firstFree m
    (e', m') = evalDecl ds e m
    deallocation (Just (e,m)) = Just (e, dealloc m p)
    deallocation Nothing = Nothing

fac :: Integer -> Command
fac n = Block [ VarDecl "n" (Literal n)
              , VarDecl "r" (Literal 1)
              , ProcDecl "step" "_" (Seq (Assign "r" (Mul (Var "r") (Var "n"))) (Assign "n" (Sub (Var "n") (Literal 1))))
              ] $
              While (Lt (Literal 0) (Var "n")) (Call "step" (Literal 666))

simpleAssign = Block [ VarDecl "x" (Literal 2)
                     , VarDecl "y" (Literal 3)
                     ] (Assign "y" (Mul (Var "x") (Var "y")))

procAssign = Block [ VarDecl "x" (Literal 0)
                   , ProcDecl "p" "n" (Assign "x" (Var "n"))
                   ] (Call "p" (Mul (Literal 20) (Literal 3)))

simpleSum n = Block [ VarDecl "n" (Literal n)
                    , VarDecl "s" (Literal 0)
                    ] $ While (Lt (Literal 0) (Var "n")) (Seq (Assign "s" (Add (Var "s") (Var "n"))) (Assign "n" (Sub (Var "n") (Literal 1))))

facRec n = Block [ VarDecl "r" (Literal 1)
                 , ProcDecl "fac" "n" (If (Lt (Var "n") (Literal 2)) (Assign "r" (Literal 1)) (Seq (Call "fac" (Sub (Var "n") (Literal 1))) (Assign "r" (Mul (Var "r") (Var "n"))) ))
                 ] $ Call "fac" (Literal n)
