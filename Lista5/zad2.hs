type Ident = String

data Aexp = Lit Integer
          | Var Ident
          | Add Aexp Aexp
          | Sub Aexp Aexp
          | Mul Aexp Aexp
          deriving (Show)

data Bexp = Const Bool
          | Eq Aexp Aexp
          | Leq Aexp Aexp
          | Not Bexp
          | Or Bexp Bexp
          | And Bexp Bexp
          deriving (Show)

data Com  = Skip
          | Seq Com Com
          | Assign Ident Aexp
          | If Bexp Com Com
          | While Bexp Com
          deriving (Show)

type State = [(Ident,Integer)]

emptyState = []
updateState s x n = (x, n) : filter (\(y,_) -> y /= x) s
getVar s x = n where
    Just n = lookup x s

aeval :: Aexp -> State -> Integer
aeval (Lit n) _ = n
aeval (Var x) s = getVar s x
aeval (Add a b) s = aeval a s + aeval b s
aeval (Sub a b) s = aeval a s - aeval b s
aeval (Mul a b) s = aeval a s * aeval b s

beval :: Bexp -> State -> Bool
beval (Const b) _ = b
beval (Eq a b) s  = aeval a s == aeval b s
beval (Leq a b) s = aeval a s <= aeval b s
beval (Not b) s   = not (beval b s)
beval (Or a b) s  = beval a s || beval b s
beval (And a b) s = beval a s && beval b s

step :: (Com, State) -> Either (Com, State) State
step (Skip, s)       = Right s
step (Assign x a, s) = Right (updateState s x (aeval a s))
step (Seq c1 c2, s)  = case step (c1, s) of
                           Left (c1', s') -> Left (Seq c1' c2, s')
                           Right s'       -> Left (c2, s')
step (If b c1 c2, s) = if beval b s
                           then step (c1, s)
                           else step (c2, s)
step (While b c, s)  = step (If b (Seq c (While b c)) Skip, s)

atMostN :: (Com, State) -> Integer -> Either Com State
atMostN (c, s) 0 = Left c
atMostN (c, s) n = case step (c, s) of
                    Left p  -> atMostN p (n-1)
                    Right s -> Right s

eval :: (Com, State) -> State
eval p = case step p of
             Left p' -> eval p'
             Right s -> s

whileTrue = While (Const True) Skip

fac n = Assign "x" (Lit 1) `Seq`
        Assign "n" (Lit n) `Seq`
        (While (Leq (Lit 1) (Var "n")) $
            Assign "x" (Mul (Var "x") (Var "n")) `Seq`
            Assign "n" (Sub (Var "n") (Lit 1)))
