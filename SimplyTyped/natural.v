Require Import Arith.
Require Import List.
Require Import Specif.

Definition var := nat.

Inductive Lambda : Set :=
| Lvar : var -> Lambda
| Labs : var -> Lambda -> Lambda
| Lapp : Lambda -> Lambda -> Lambda.

Notation "[ x ]" := (Lvar x) (at level 0).
Notation "x --> t" := (Labs x t) (at level 30).
Notation "m @ n" := (Lapp m n) (at level 20).

Definition nvalue (t:Lambda) := exists (x:var) (t':Lambda), t = x --> t'.

Inductive Env : Set :=
| EEmpty : Env
| ECons  : var -> (Lambda * Env) -> Env -> Env. 

Definition NClosure := (Lambda * Env)%type.

Fixpoint search_env (e:Env) (x:var) : option NClosure :=
match e with
| EEmpty       => None
| ECons y c e' => if eq_nat_dec x y then Some c else search_env e' x
end.

Inductive ValuedEnv : Env -> Prop :=
| ValuedEmpty : ValuedEnv EEmpty
| ValuedCons  : forall (x y:var) (t:Lambda) (e e':Env), ValuedEnv e -> ValuedEnv e' ->
                                                        ValuedEnv (ECons x (y --> t, e') e).

Lemma search_env_returns_value :
  forall (e:Env) (x:var) (c:NClosure), ValuedEnv e -> search_env e x = Some c -> nvalue (fst c).
Proof.
induction 1; unfold nvalue in *; simpl; eauto; intuition.
inversion H.
case_eq (eq_nat_dec x x0); intros; subst; simpl in *; eauto.
rewrite H2 in H1; simpl in H1.
injection H1; intros.
exists y.
exists t.
rewrite <- H3.
simpl; auto.
rewrite H2 in H1; simpl in H1.
apply IHValuedEnv1; auto.
Qed.

Lemma valued_env_contains_valued_env :
  forall (e e':Env) (x:var) (t:Lambda), ValuedEnv e -> ValuedEnv e -> search_env e x = Some (t, e') -> ValuedEnv e'.
Proof.
induction 1; intros; simpl in *; eauto; intuition.
inversion H0.
case_eq (eq_nat_dec x x0); intros; subst; simpl in *.
rewrite H5 in H2; simpl in H2.
injection H2; intros; subst; auto.

rewrite H5 in H2; simpl in H2.
apply H3; auto.
Qed.

(* We are dealing only with closed terms, so only values are abstractions. *)
Inductive Red : (Lambda * Env) -> (Lambda * Env) -> Prop :=
| Rvar : forall (e e':Env) (x:var) (t t':Lambda), search_env e x = Some (t, e') -> Red ([x], e) (t, e')
| Rabs : forall (e:Env) (x:var) (t:Lambda), Red (x --> t, e) (x --> t, e)
| Rapp : forall (e e1 e2 e3:Env) (x:var) (t t0 t1 t2 t3:Lambda),
                 Red (t0, e) (x --> t, e1) -> Red (t1, e) (t2, e2) -> Red (t, ECons x (t2,e2) e) (t3, e3) ->
                 Red (t0 @ t1, e) (t3, e3).

Inductive MachineCmd : Set :=
| Apply   : MachineCmd
| Return  : MachineCmd
| Access  : nat -> MachineCmd
| Closure : Machine -> MachineCmd
with Machine : Set :=
| Empty : Machine
| Cons  : MachineCmd -> Machine -> Machine.

Inductive MEnv : Set :=
| MEmpty : MEnv
| MCons  : (Machine * MEnv) -> MEnv -> MEnv.

Definition MClosure := (Machine * MEnv)%type.

Fixpoint search_menv (e:MEnv) (n:nat) : option MClosure :=
match e with
| MEmpty     => None
| MCons c e' => if eq_nat_dec n 0 then Some c else search_menv e' (pred n)
end.

Definition Stack := list MClosure.
Definition Conf := (Machine * MEnv * Stack)%type.

Inductive Step : Conf -> Conf -> Prop :=
| SAccess  : forall (n:nat) (e:MEnv) (s:Stack) (r:Machine) (c:MClosure),
               search_menv e (pred n) = Some c -> Step (Cons (Access n) r, e, s) (r, e, c :: s)
| SReturn  : forall (e e':MEnv) (r r':Machine) (s:Stack) (c:MClosure),
               Step (Cons Return r, e, c :: (r', e') :: s) (r', e', c :: s)
| SApply   : forall (e e':MEnv) (r r':Machine) (s:Stack) (c:MClosure),
               Step (Cons Apply r, e, c :: (r', e') :: s) (r', MCons c e', (r, e) :: s)
| SClosure : forall (e: MEnv) (r r':Machine) (s:Stack),
               Step (Cons (Closure r') r, e, s) (r, e, (r', e) :: s).

Inductive Steps : Conf -> Conf -> Prop :=
| SZero : forall (c:Conf), Steps c c
| SStep : forall (c0 c1 c2:Conf), Step c0 c1 -> Steps c1 c2 -> Steps c0 c2.

Definition mvalue (m:Machine) :=
  exists (m':Machine), m = Cons (Closure m') Empty.

Inductive ValuedMEnv : MEnv -> Set :=
| MValuedEmpty : ValuedMEnv MEmpty
| MValuedCons  : forall (m:Machine) (e0 e1:MEnv), ValuedMEnv e0 -> ValuedMEnv e1 ->
                                                  ValuedMEnv (MCons (Cons (Closure m) Empty, e0) e1).

Inductive Mred : Conf -> MClosure -> Prop :=
| MredVal : forall (c:Conf) (v:Machine) (e e':MEnv) (s:Stack),
              Steps c (Empty, e, (v, e') :: s) -> mvalue v -> Mred c (v, e').

Fixpoint FV (t:Lambda) : list var :=
match t with
| [v]     => v :: nil
| x --> e => remove eq_nat_dec x (FV e)
| m @ n   => FV m ++ FV n
end.

Definition closed (t:Lambda) := FV t = nil.

Lemma lambda_is_value :
  forall (c0 c1 : NClosure), Red c0 c1 -> ValuedEnv (snd c0) -> nvalue (fst c1) /\ ValuedEnv (snd c1).
Proof.
induction 1; unfold nvalue in *; simpl; intros; eauto.
specialize (search_env_returns_value e x (t, e') H0 H); intros.
unfold nvalue in H1.
destruct H1; destruct H1; simpl in H1.
split.
exists x0; exists x1; auto.

inversion H0; subst.
simpl in H.
inversion H.
specialize valued_env_contains_valued_env; intros.
eapply H1.
exact H0.
exact H0.
exact H.

split.
simpl in *.
apply IHRed3.
intuition.
destruct H6.
destruct H3.
rewrite H3.
apply ValuedCons; auto.

simpl in *.
apply IHRed3.
intuition.
destruct H6.
destruct H3.
rewrite H3.
apply ValuedCons; auto.
Qed.

(* 1 + position on list, 0 otherwise. *)
Fixpoint find_pos (vs : list var) (x : var) : nat :=
match vs with
| nil     => 0
| v :: vs => if eq_nat_dec x v then 1 else S (find_pos vs x)
end.

Definition free_mapping (vs : list var) : var -> nat := find_pos vs.

Fixpoint machine_append (m1:Machine) (m2:Machine) : Machine :=
match m1 with
| Empty     => m2
| Cons m ms => Cons m (machine_append ms m2)
end.

Fixpoint translate (t:Lambda) (fv : list var) : Machine :=
match t with
| [v]     => Cons (Access (find_pos fv v)) Empty
| x --> t => Cons (Closure (machine_append (translate t (x :: fv)) (Cons Return Empty))) Empty
| m @ n   => machine_append (machine_append (translate m fv) (translate n fv)) (Cons Apply Empty)
end.

Definition translate_start (t:Lambda) := translate t (FV t).

Definition At (vs : list var) (x : var) (n : nat) :=
  nth_error vs n = Some x.

Fixpoint FI (m:Machine) : list nat :=
match m with
| Empty       => nil
| Cons cmd cs => FICmd cmd ++ FI cs
end
with FICmd (c:MachineCmd) : list nat :=
match c with
| Apply      => nil
| Return     => nil
| Closure cs => filter (fun x => if eq_nat_dec 0 x then false else true) (map pred (FI cs))
| Access n   => n :: nil
end.

Inductive EnvCompat : Lambda -> Env -> MEnv -> Prop :=
| EnvCompatibility  : forall (t:Lambda) (e:Env) (m:MEnv),
                        (forall (x:var) (n:nat) (c0:NClosure) (c1:MClosure),
                         At (FV t) x n ->
                         (search_env e x = Some c0 <-> search_menv m n = Some c1) /\
                         translate_start (fst c0) = Cons (Closure (fst c1)) Empty /\
                         EnvCompat (fst c0) (snd c0) (snd c1)) ->
                        EnvCompat t e m.

Definition values_compat (c0:NClosure) (c1:MClosure) :=
  translate_start (fst c0) = Cons (Closure (fst c1)) Empty /\ EnvCompat (fst c0) (snd c0) (snd c1).

Lemma mred_is_deterministic :
  forall (c:Conf) (v v0:Machine) (e e0 e' e0':MEnv) (s s':Stack),
         Steps c (Empty, e, (v, e') :: s) -> mvalue v ->
         Steps c (Empty, e0, (v0, e0') :: s') -> mvalue v0 ->
         e = e0 /\ v = v0 /\ e' = e0' /\ s = s'.
Proof.
Admitted.

Lemma eq_nat_dec_if :
  forall (n a b:nat), (if eq_nat_dec n n then a else b) = a.
Proof.
intros.
case_eq (eq_nat_dec n n); intros; eauto.
assert (n = n) by auto.
contradiction.
Qed.

Theorem machine_compatibility :
  forall (v v0:NClosure),
    Red v v0 ->
    forall (t:Lambda) (e:Env) (me:MEnv) (s:Stack),
      v = (t, e) ->
      ValuedEnv e ->
      ValuedMEnv me ->
      EnvCompat t e me ->
      exists (v1:MClosure),
        Steps (translate_start t, me, s) (Empty, snd v1, v1 :: s) /\
        values_compat v0 v1.
Proof.
induction 1; intros; subst.
injection H0; intros; subst; unfold translate_start in *; simpl in *.
clear H0.

inversion H3; subst.
pose (v1 := (Cons (Closure (Cons (Access 0) (Cons Return Empty))) Empty, me)).
specialize (H0 x 0 (t, e') v1).
unfold At in H0.
simpl in H0.
unfold value in H0.
specialize (H0 eq_refl).
intuition.
exists v1.
split.

eapply SStep.
eapply SAccess.
rewrite eq_nat_dec_if; simpl.
exact H4.
apply SZero.
unfold values_compat.
simpl; split; eauto.

injection H; intros; subst; clear H.
pose (v1 := (machine_append (translate t (x :: remove eq_nat_dec x (FV t))) (Cons Return Empty), me)).
exists v1.
unfold translate_start in *; simpl in *; split; subst; eauto.

eapply SStep.
constructor.
apply SZero.

unfold values_compat; unfold translate_start; simpl; split; eauto.

injection H2; intros; subst; clear H2.
assert (EnvCompat t0 e0 me) by admit.
specialize (IHRed1 t0 e0 me s eq_refl H3 H4 H2).
destruct IHRed1.
destruct H6.
unfold values_compat in H7; simpl in H7.
destruct H7.

assert (EnvCompat t1 e0 me) by admit.
specialize (IHRed2 t1 e0 me s eq_refl H3 H4 H9).
destruct IHRed2.
unfold translate_start in H10; unfold values_compat in H10; simpl in H10; destruct H10.
destruct H11.

assert (ValuedEnv (ECons x (t2, e2) e0)) by admit.
assert (ValuedMEnv (MCons x1 me)) by admit.
assert (EnvCompat t (ECons x (t2, e2) e0) (MCons x1 me)) by admit.
specialize (IHRed3 t (ECons x (t2, e2) e0) (MCons x1 me) s eq_refl H13 H14 H15).

destruct IHRed3.
unfold translate_start in H16; unfold values_compat in H16; simpl in H16; destruct H16.
destruct H17.

exists x2.
unfold values_compat; unfold translate_start; simpl; intuition; eauto.

(* To show: 'append' on Steps, then with careful stack manipulation one
   can show the goal! *)

Admitted.