Require Import Arith.
Require Import List.
Require Import Specif.

Definition var := nat.

Inductive Lambda : Set :=
| Lvar : var -> Lambda
| Labs : var -> Lambda -> Lambda
| Lapp : Lambda -> Lambda -> Lambda.

Notation "[ x ]" := (Lvar x) (at level 0).
Notation "x --> t" := (Labs x t) (at level 30).
Notation "m @ n" := (Lapp m n) (at level 20).

Inductive Env : Set :=
| EEmpty : Env
| ESkip  : var -> Env -> Env
| ECons  : var -> NClosure -> Env -> Env
with NClosure : Set :=
| NMakeClosure : Lambda -> Env -> NClosure.

Definition term (c:NClosure) :=
match c with
| NMakeClosure t _ => t
end.

Definition env (c:NClosure) :=
match c with
| NMakeClosure _ e => e
end.

Fixpoint search_env (e:Env) (x:var) : option NClosure :=
match e with
| EEmpty       => None
| ESkip y e'   => if eq_nat_dec x y then None else search_env e' x
| ECons y c e' => if eq_nat_dec x y then Some c else search_env e' x
end.

Inductive ClosedClosure : NClosure -> Lambda -> Prop :=
| ClosedVar : forall (x:var) (e:Env) (c:NClosure) (t:Lambda),
                search_env e x = Some c ->
                ClosedClosure c t ->
                ClosedClosure (NMakeClosure [x] e) t
| ClosedLambda : forall (x:var) (t0 t1:Lambda) (e:Env),
                   ClosedClosure (NMakeClosure t0 (ESkip x e)) t1 ->
                   ClosedClosure (NMakeClosure (x --> t0) e) (x --> t1)
| ClosedApp : forall (t0 t1 t0' t1':Lambda) (e:Env),
                ClosedClosure (NMakeClosure t0 e) t0' ->
                ClosedClosure (NMakeClosure t1 e) t1' ->
                ClosedClosure (NMakeClosure (t0 @ t1) e) (t0' @ t1').

Definition is_lambda (t:Lambda) :=
  exists (x:var) (t':Lambda), t = x --> t'.

Definition nvalue (c:NClosure) :=
  exists v, ClosedClosure c v /\ is_lambda v.

Definition valued_env (e:Env) :=
  forall (x:var) (c:NClosure), search_env e x = Some c -> nvalue c.

(* We are dealing only with closed terms, so only values are abstractions. *)
Inductive Red : NClosure -> NClosure -> Prop :=
| Rvar : forall (e e':Env) (x:var) (t t':Lambda), search_env e x = Some (NMakeClosure t e') ->
                                                  Red (NMakeClosure ([x]) e) (NMakeClosure t e')
| Rabs : forall (e:Env) (x:var) (t:Lambda), Red (NMakeClosure (x --> t) e) (NMakeClosure (x --> t) e)
| Rapp : forall (e e1 e2:Env) (x:var) (t t0 t1 t2:Lambda) (c2 c3:NClosure),
                 Red (NMakeClosure t0 e) (NMakeClosure (x --> t) e1) ->
                 Red (NMakeClosure t1 e) c2 ->
                 Red (NMakeClosure t (ECons x c2 e)) c3 ->
                 Red (NMakeClosure (t0 @ t1) e) c3.

Inductive MachineCmd : Set :=
| Apply   : MachineCmd
| Return  : MachineCmd
| Access  : nat -> MachineCmd
| Closure : Machine -> MachineCmd
with Machine : Set :=
| Empty : Machine
| Cons  : MachineCmd -> Machine -> Machine.

Inductive MEnv : Set :=
| MEmpty : MEnv
| MSkip  : MEnv -> MEnv
| MCons  : MClosure -> MEnv -> MEnv
with MClosure : Set :=
| MMakeClosure : Machine -> MEnv -> MClosure.

Definition machine (c:MClosure) :=
match c with
| MMakeClosure m _ => m
end.

Definition menv (c:MClosure) :=
match c with
| MMakeClosure _ e => e
end.

Fixpoint search_menv (e:MEnv) (n:nat) : option MClosure :=
match e with
| MEmpty     => None
| MSkip m'   => if eq_nat_dec n 0 then None else search_menv m' (pred n)
| MCons c e' => if eq_nat_dec n 0 then Some c else search_menv e' (pred n)
end.

Fixpoint machine_append (m1:Machine) (m2:Machine) : Machine :=
match m1 with
| Empty     => m2
| Cons m ms => Cons m (machine_append ms m2)
end.

Inductive MClosedClosureCmd : MachineCmd -> MEnv -> Machine -> Prop :=
| MClosedApply  : forall (e:MEnv), MClosedClosureCmd Apply e (Cons Apply Empty)
| MClosedReturn : forall (e:MEnv), MClosedClosureCmd Return e (Cons Return Empty)
| MClosedAccess : forall (e:MEnv) (n:nat) (c:MClosure) (m:Machine),
                       search_menv e n = Some c ->
                       MClosedClosure c m ->
                       MClosedClosureCmd (Access n) e m
| MClosedClosureV : forall (e:MEnv) (m m':Machine),
                        MClosedClosure (MMakeClosure m e) m' ->
                        MClosedClosureCmd (Closure m) e m'
with MClosedClosure : MClosure -> Machine -> Prop :=
| MClosedEmpty : forall (e:MEnv), MClosedClosure (MMakeClosure Empty e) Empty
| MClosedCons  : forall (e:MEnv) (c:MachineCmd) (m m0 m1:Machine),
                   MClosedClosureCmd c e m0 ->
                   MClosedClosure (MMakeClosure m e) m1 ->
                   MClosedClosure (MMakeClosure (Cons c m) e) (machine_append m0 m1).

Definition is_closure (m:Machine) :=
  exists m', m = Cons (Closure m') Empty.

Definition mvalue (c:MClosure) :=
  exists (m:Machine), MClosedClosure c m /\ is_closure m.

Definition valued_menv (e:MEnv) :=
  forall (n:nat) (c:MClosure), search_menv e n = Some c -> mvalue c.

Definition Stack := list MClosure.
Definition Conf := (Machine * MEnv * Stack)%type.

Inductive Step : Conf -> Conf -> Prop :=
| SAccess  : forall (n:nat) (e:MEnv) (s:Stack) (r:Machine) (c:MClosure),
               search_menv e (pred n) = Some c -> Step (Cons (Access n) r, e, s) (r, e, c :: s)
| SReturn  : forall (e e':MEnv) (r r':Machine) (s:Stack) (c:MClosure),
               Step (Cons Return r, e, c :: (MMakeClosure r' e') :: s) (r', e', c :: s)
| SApply   : forall (e e':MEnv) (r r':Machine) (s:Stack) (c:MClosure),
               Step (Cons Apply r, e, c :: (MMakeClosure r' e') :: s) (r', MCons c e', (MMakeClosure r e) :: s)
| SClosure : forall (e: MEnv) (r r':Machine) (s:Stack),
               Step (Cons (Closure r') r, e, s) (r, e, (MMakeClosure r' e) :: s).

Inductive Steps : Conf -> Conf -> Prop :=
| SZero : forall (c:Conf), Steps c c
| SStep : forall (c0 c1 c2:Conf), Step c0 c1 -> Steps c1 c2 -> Steps c0 c2.

Fixpoint FV (t:Lambda) : list var :=
match t with
| [v]     => v :: nil
| x --> e => remove eq_nat_dec x (FV e)
| m @ n   => FV m ++ FV n
end.

Lemma closed_closure_is_closed :
  forall (c:NClosure) (t:Lambda),
    ClosedClosure c t -> FV t = nil.
Proof.
induction 1; intros; simpl in *; eauto; intuition.
rewrite IHClosedClosure.
simpl; auto.
rewrite IHClosedClosure1; rewrite IHClosedClosure2; simpl; auto.
Qed.

Fixpoint lift {A B:Set} (f:A -> B) (x:option A) : option B :=
match x with
| None => None
| Some x => Some (f x)
end.

Fixpoint lift2 {A B C:Set} (f:A -> B -> C) (x:option A) (y:option B) : option C :=
match x with
| None => None
| Some x => match y with
            | None => None
            | Some y => Some (f x y)
            end
end.

(* Some $ 1 + position on list, None otherwise. *)
Fixpoint find_pos (vs : list var) (x : var) : option nat :=
match vs with
| nil     => None
| v :: vs => if eq_nat_dec x v then Some 1 else lift S (find_pos vs x)
end.


Fixpoint translate (t:Lambda) (fv : list var) : option Machine :=
match t with
| [v]     => lift (fun n => Cons (Access n) Empty) (find_pos fv v)
| x --> t => lift (fun m => Cons (Closure (machine_append m (Cons Return Empty))) Empty) (translate t (x::fv))
| m @ n   => lift2 (fun m n => machine_append (machine_append m n) (Cons Apply Empty))
                   (translate m fv) (translate n fv)
end.

Definition translate_start (t:Lambda) := translate t (FV t).

Definition equivalent (c0:NClosure) (c1:MClosure) :=
  exists (t:Lambda) (m:Machine),
    ClosedClosure c0 t ->
    MClosedClosure c1 m ->
    translate_start t = Some m.

Lemma steps_snoc :
  forall (c0 c1 c2:Conf), Steps c0 c1 -> Step c1 c2 -> Steps c0 c2.
Proof.
induction 1; intros; eauto; intuition.
apply SStep with (c1 := c2); auto.
apply SZero.

apply SStep with (c1 := c1); auto.
Qed.

Lemma steps_append :
  forall (c1 c2:Conf), Steps c1 c2 -> forall (c0:Conf), Steps c0 c1 -> Steps c0 c2.
Proof.
induction 1; intros; simpl in *; eauto.
assert (Steps c3 c1).
specialize (steps_snoc c3 c0 c1 H1 H); eauto.
apply IHSteps; auto.
Qed.

Theorem machine_compatibility :
  forall (v v0:NClosure),
    Red v v0 ->
    forall (c0:MClosure),
      equivalent v c0 ->
      valued_env (env v) ->
      valued_menv (menv c0) ->
      exists (c1:MClosure),
        forall (m':Machine) (s:Stack),
          Steps (machine_append (machine c0) m', menv c0, s) (m', menv c1, c1 :: s) /\
          equivalent v0 c1.
Proof.
induction 1; intros; eauto; simpl in *; eauto; intuition.
