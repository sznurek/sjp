module Parser where

import Terms
import Control.Applicative hiding (many)
import Control.Monad
import Data.Char

import Debug.Trace

data Parser k s a = Step (k -> [s] -> ([s], k, Parser k s a))
                  | Or (Parser k s a) (Parser k s a)
                  | Done (k -> (k, a))
                  | NoMatch
                  | Error String

map3 :: (a -> b) -> (x, y, a) -> (x, y, b)
map3 f (a, b, c) = (a, b, f c)

instance Functor (Parser k s) where
    fmap f (Step s)   = Step (\k c -> map3 (fmap f) (s k c))
    fmap f (Or p0 p1) = Or (fmap f p0) (fmap f p1)
    fmap f (Done g)   = Done (\k -> fmap f (g k))
    fmap f NoMatch    = NoMatch
    fmap f (Error s)  = Error s

instance Monad (Parser k s) where
    return x = Done (\k -> (k, x))
    Step s   >>= f = Step (\k c -> map3 (>>= f) (s k c))
    Or p0 p1 >>= f = Or (p0 >>= f) (p1 >>= f)
    Done g   >>= f = Step (\k c -> let (k', a) = g k in (c, k', f a))
    NoMatch  >>= _ = NoMatch
    Error s  >>= _ = Error s

instance Applicative (Parser k s) where
    pure = return
    fa <*> xa = do f <- fa
                   x <- xa
                   return (f x)

instance Alternative (Parser k s) where
    empty = NoMatch
    (<|>) = Or

get :: Parser k s k
get = Done (\k -> (k, k))

put :: k -> Parser k s ()
put k = Done (\_ -> (k, ()))

modify :: (k -> k) -> Parser k s k
modify f = Done (\k -> (f k, k))

runParser :: Parser k s a -> k -> [s] -> ([s], k, Parser k s a)
runParser (Step s) k cs = runParser p k' cs' where
    (cs', k', p) = s k cs
runParser (Or p0 p1) k cs = case runParser p0 k cs of
    (s, k, Done a)  -> (s, k, Done a)
    (s, k, Error m) -> (s, k, Error m)
    (s, _, NoMatch) -> runParser p1 k cs
runParser p k cs = (cs, k, p)

parse p k cs = case runParser p k cs of
    (s, k, Done g)   -> Right (s, snd (g k))
    (_, _, Error s)  -> Left ("Error: " ++ s)
    (_, _, NoMatch ) -> Left "No match"


char :: Eq a => a -> Parser k a a
char c = matching (==c)

oneOf :: Eq a => [a] -> Parser k a a
oneOf cs = matching (`elem` cs)

matching :: (a -> Bool) -> Parser k a a
matching f = Step aux where
    aux k []     = ([], k, NoMatch)
    aux k (c:cs) = if f c then (cs, k, return c) else (c:cs, k, NoMatch)

exact []     = return ()
exact (x:xs) = char x >> exact xs

many1 :: Parser k s a -> Parser k s [a]
many1 p = do x  <- p
             xs <- many p
             return (x:xs)

many p = many1 p <|> return []

skipMany p = many p >> return ()
skipMany1 p = many1 p >> return ()

spaces = skipMany (matching isSpace)

digit = matching isDigit

sepBy p sep = many (sep >> p) >> sep


data Store = Store Integer (Var -> Maybe Var)
type FunParser a = Parser Store Char a

funParse :: String -> Either String (String, Expr)
funParse prog = parse expression (Store 0 (const Nothing)) prog

genSym :: FunParser Var
genSym = Done (\(Store k f) -> (Store (k+1) f, "gensym_" ++ show k))

refresh :: Var -> FunParser Var
refresh x = do (Store k f) <- get
               case f x of
                 Nothing -> do y <- genSym
                               modify (\(Store k f) -> Store k (\z -> if z == x then Just y else f z))
                               return y
                 Just y  -> return y

number = read <$> many1 digit
boolean = (exact "true" >> return True) <|>
          (exact "false" >> return False)

ident = many1 (matching isAlpha)


inside p c0 c1 = do
    char c0
    spaces
    x <- p
    spaces
    char c1
    return x

inParens p = inside p '(' ')'

expression = (Value <$> value) <|>
             (Variable <$> (ident >>= refresh)) <|>
             inParens (
                 ifExpr <|>
                 op1 <|>
                 op2 <|>
                 letExpr <|>
                 fstExpr <|>
                 sndExpr <|>
                 caseExpr <|>
                 app)

value = (Number <$> number) <|>
        (Boolean <$> boolean) <|>
        (exact "unit" >> return Unit) <|>
        (inParens (abstraction <|> pair <|> inl <|> inr))

op1 = (Op1 OpMinus1 <$> (exact "minus" >> spaces >> expression)) <|>
      (Op1 OpNegate <$> (exact "negate" >> spaces >> expression))

op2 = (Op2 (Op2Arith OpPlus) <$> (exact "+" >> spaces >> expression) <*> (spaces >> expression)) <|>
      (Op2 (Op2Arith OpMinus) <$> (exact "-" >> spaces >> expression) <*> (spaces >> expression)) <|>
      (Op2 (Op2Arith OpMult) <$> (exact "*" >> spaces >> expression) <*> (spaces >> expression)) <|>
      (Op2 (Op2Arith OpDiv) <$> (exact "/" >> spaces >> expression) <*> (spaces >> expression)) <|>
      (Op2 (Op2Arith OpLess) <$> (exact "<" >> spaces >> expression) <*> (spaces >> expression)) <|>
      (Op2 (Op2Bool OpAnd) <$> (exact "and" >> spaces >> expression) <*> (spaces >> expression)) <|>
      (Op2 (Op2Bool OpOr) <$> (exact "or" >> spaces >> expression) <*> (spaces >> expression)) <|>
      (Op2 OpEquals <$> (exact "==" >> spaces >> expression) <*> (spaces >> expression))

ifExpr = do exact "if"
            spaces
            cond <- expression
            spaces
            e0 <- expression
            spaces
            e1 <- expression
            return (If cond e0 e1)

app = do e0 <- expression
         spaces
         e1 <- expression
         x <- genSym
         return (Let x e1 (App e0 x))

letExpr = do exact "let"
             spaces
             x <- ident >>= refresh
             spaces
             e0 <- expression
             spaces
             e1 <- expression
             return (Let x e0 e1)

fstExpr = do exact "fst"
             spaces
             e <- expression
             return (Fst e)

sndExpr = do exact "snd"
             spaces
             e <- expression
             return (Snd e)

caseExpr = do exact "case"
              spaces
              e <- expression
              spaces
              x0 <- ident >>= refresh
              spaces
              e0 <- expression
              spaces
              x1 <- ident >>= refresh
              spaces
              e1 <- expression
              return (Case e x0 e0 x1 e1)

inl = do exact "inl"
         spaces
         e <- expression
         return (Inl e)

inr = do exact "inr"
         spaces
         e <- expression
         return (Inr e)

pair = do exact "pair"
          spaces
          e0 <- expression
          spaces
          e1 <- expression
          return (Pair e0 e1)

abstraction = do exact "lambda"
                 spaces
                 x <- ident >>= refresh
                 spaces
                 e <- expression
                 return (Abs x e)

