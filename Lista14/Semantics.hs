module Semantics where

import Terms
import Debug.Trace

data Storable = Thunk Env Expr
              | Computed Value

type Env = [(Var,Integer)]
type Mem = (Integer, [(Integer,Storable)])

type Store = (Env, Mem)

emptyStore = ([], (0, []))
insertThunk (env, (k, mem)) x e = ((x,k) : env, (k+1, (k, Thunk ((x, k) : env) e) : mem))

link (env, (k, mem)) x y = case lookup y env of
    Just i -> ((x, i) : env, (k, mem))
    Nothing -> error ("How to link " ++ x ++ " to " ++ y ++ "?")

getComputed (env, (k, mem)) x = case lookup x env of
    Just i -> case lookup i mem of
                  Just (Thunk env' e) -> let (v, (envn, (kn, memn))) = eval (env', (k, mem)) e in (v, (env, (k, (i, Computed v) : memn)))
                  Just (Computed v)   -> (v, (env, (k, mem)))

eval :: Store -> Expr -> (Value, Store)
eval s (Value v)    = (v, s)
eval s (Variable x) = getComputed s x

eval s (If cond e0 e1) = if b then eval s' e0 else eval s' e1 where
    (Boolean b, s') = eval s cond

eval s (App e x) = eval s'' e' where
    (Abs y e', s') = eval s e
    s'' = link s' y x

eval s (Let x e0 e1) = eval s' e1 where
    s' = insertThunk s x e0

eval s (Fst e) = eval s' e0 where
    (Pair e0 _, s') = eval s e

eval s (Snd e) = eval s' e1 where
    (Pair _ e1, s') = eval s e

eval s (Case e x0 e0 x1 e1) = case eval s e of
    (Inl e, s') -> eval (insertThunk s' x0 e) e0
    (Inr e, s') -> eval (insertThunk s' x1 e) e1

eval s (Op1 OpMinus1 e) = case eval s e of
    (Number n, s') -> (Number (-n), s')

eval s (Op1 OpNegate e) = case eval s e of
    (Boolean b, s') -> (Boolean (not b), s')

eval s (Op2 (Op2Arith op) e0 e1) =
    let (v0, s')  = eval s e0
        (v1, s'') = eval s' e1
    in  case (v0, v1) of
            (Number n0, Number n1) -> (evalArith op n0 n1, s'')

eval s (Op2 (Op2Bool op) e0 e1) =
    let (v0, s')  = eval s e0
        (v1, s'') = eval s' e1
    in  case (v0, v1) of
            (Boolean b0, Boolean b1) -> (evalBool op b0 b1, s'')

eval s (Op2 OpEquals e0 e1) =
    let (v0, s')  = eval s e0
        (v1, s'') = eval s' e1
    in  (case (v0, v1) of
            (Unit, Unit)           -> Boolean True
            (Unit, _)              -> Boolean False
            (_, Unit)              -> Boolean False
            (Number n0, Number n1) -> Boolean (n0 == n1), s'')

evalArith OpPlus  x y = Number (x+y)
evalArith OpMinus x y = Number (x-y)
evalArith OpMult  x y = Number (x*y)
evalArith OpDiv   x y = Number (x `div` y)
evalArith OpLess  x y = Boolean (x < y)

evalBool OpAnd x y = Boolean (x && y)
evalBool OpOr  x y = Boolean (x || y)

