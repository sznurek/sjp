module Terms where

type Var = String

data Value = Number Integer
           | Boolean Bool
           | Unit
           | Abs Var Expr
           | Pair Expr Expr
           | Inl Expr
           | Inr Expr deriving (Show)

data Expr = Value Value
          | Variable Var
          | If Expr Expr Expr
          | Op1 Op1 Expr
          | Op2 Op2 Expr Expr
          | App Expr Var
          | Let Var Expr Expr
          | Fst Expr
          | Snd Expr
          | Case Expr Var Expr Var Expr deriving (Show)

data Op1 = OpMinus1
         | OpNegate deriving (Show)

data Op2Arith = OpPlus
              | OpMinus
              | OpMult
              | OpDiv
              | OpLess deriving (Show)

data Op2Bool = OpAnd
             | OpOr deriving (Show)

data Op2 = Op2Arith Op2Arith
         | Op2Bool Op2Bool
         | OpEquals deriving (Show)

