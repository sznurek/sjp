module Main where

import Terms
import Parser
import Semantics


run :: String -> Value
run prog = fst (eval emptyStore expr) where
    Right (_, expr) = funParse prog

fac5 = "(let fac (lambda n (if (== n 0) 1 (* n (fac (- n 1))))) (fac 5))"
soLazy = "(let omega omega (if true 1 omega))"
recExample = "(let x (pair 1 x) (fst (snd (snd x))))"

